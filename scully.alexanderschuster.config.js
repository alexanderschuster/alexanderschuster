exports.config = {
  projectRoot: './src',
  projectName: 'alexanderschuster',
  outDir: './dist/static',
  routes: {
    '/blog/:slug': {
      type: 'contentFolder',
      slug: {
        folder: './blog'
      }
    }
  }
};
