import { Component, OnInit } from '@angular/core';
import { ScullyRoute, ScullyRoutesService } from '@scullyio/ng-lib';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'as-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {
  routes$: Observable<ScullyRoute[]>;

  constructor(private scully: ScullyRoutesService) {}

  ngOnInit(): void {
    this.routes$ = this.scully.available$.pipe(
      map(availableRoutes => {
        const blogRoutes = [];
        availableRoutes.forEach(route =>
          route.title ? blogRoutes.push(route) : ''
        );
        return blogRoutes;
      })
    );
  }
}
